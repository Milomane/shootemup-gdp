﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;
    public int score;
    public List<Profil> allProfils;
    private string name;
    
    void Start()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        
        allProfils = new List<Profil>();
    }

    void Update()
    {
        if (FindObjectOfType<ScoreSystem>())
            score = ScoreSystem.score;
        
        

        if (GameObject.FindGameObjectWithTag("ScoreContainer"))
        {
            DrawScore();
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (GameObject.FindGameObjectWithTag("NameContainer"))
            {
                name = GameObject.FindGameObjectWithTag("NameContainer").GetComponent<TMP_InputField>().text;
                ValideName();
                SceneManager.LoadScene("EndGame");
            }
        }
        
    }

    public void ValideName()
    {
        if (allProfils.Count == 0)
        {
            allProfils.Add(new Profil(name, score));
            Debug.Log(allProfils[0].name + "    " + allProfils[0].score);
            return;
        }
        else
        {
            for (int i = 0; i < allProfils.Count; i++)
            {
                if (score > allProfils[i].score)
                {
                    allProfils.Insert(i, new Profil(name, score));
                    return;
                }
            }
            allProfils.Add(new Profil(name, score));
        }
    }
    
    public class Profil
    {
        public Profil(string inputName, int newScore)
        {
            score = newScore;
            name = inputName;
        }
        
        public int score;
        public string name;
    }

    public void DrawScore()
    {
        string textScore = "";
        
        for (int i = 0; i < allProfils.Count; i++)
        {
            textScore += i + 1 + "- " + allProfils[i].name + "  : " + allProfils[i].score + "\n";
            GameObject.FindGameObjectWithTag("ScoreContainer").GetComponent<TextMeshProUGUI>().text = textScore;
        }
    }
}