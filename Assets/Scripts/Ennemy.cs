﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Ennemy : MonoBehaviour
{
    public float speed;
    public float dmg;
    public float hp;

    public float timerShoot;
    private float TimerS;

    public GameObject explosion;
    
    void Start()
    {
        
    }
    
    public virtual void Update()
    {
        TimerS -= Time.deltaTime;

        if (TimerS <= 0)
        {
            Shoot();
            TimerS = timerShoot;
        }
    }

    public virtual void Shoot()
    {
        
    }

    public void TakeDamage(float dmg)
    {
        hp -= dmg;

        if (hp <= 0)
        {
            ScoreSystem.AddScore(1);
            Death();
        }
    }
    
    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<BulletPlayer>())
        {
            TakeDamage(dmg);
        }

        PlayerSysteme playerSysteme = col.GetComponent<PlayerSysteme>();
        
        if (playerSysteme)
        {
            playerSysteme.TakeDamage((int) dmg);
            Death();
        }
    }

    public void Death()
    {
        GameObject e = Instantiate(explosion, transform.position, explosion.transform.rotation);
        Destroy(e, 0.2f);
        Destroy(gameObject);
    }
}
