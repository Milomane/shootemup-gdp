﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
    public Vector2 speed = new Vector2(0, 2);
    private Vector2 textureOffset;
    
    void Start()
    {
        textureOffset = GetComponent<MeshRenderer>().material.mainTextureOffset;
    }
    
    void Update()
    {
        textureOffset += speed * Time.deltaTime;
        GetComponent<MeshRenderer>().material.mainTextureOffset = textureOffset;
    }
}
