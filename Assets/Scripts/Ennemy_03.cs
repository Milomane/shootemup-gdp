﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ennemy_03 : Ennemy
{
    public Vector2 direction;
    
    public GameObject bullet;
    
    private float t;
    public float oscilation;
    
    void Start()
    {
        
    }
    
    public override void Update()
    {
        base.Update();
        t += Time.deltaTime * oscilation;
        direction.y = Mathf.Cos(t);
        transform.Translate(direction * speed * 0.1f);
    }
    
    public override void Shoot()
    {
        Bullet_Enemy bulletCopy = Instantiate(bullet, transform.position, Quaternion.identity).GetComponent<Bullet_Enemy>();

        bulletCopy.direction = direction;
        bulletCopy.speed = speed * 1.2f;
        bulletCopy.dmg = dmg;
    }
}
