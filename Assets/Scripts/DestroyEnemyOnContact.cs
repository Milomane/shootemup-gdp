﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEnemyOnContact : MonoBehaviour
{
    void Start()
    {
        
    }
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Ennemy>())
        {
            Destroy(other.gameObject);
        }
    }
}
