﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject[] enemies;
    public float timeBetweenEnenies = 2f;

    public float betweenYCoord;
    public float xCoord;
    
    void Start()
    {
        InvokeRepeating("SpawnEnemy", 0, timeBetweenEnenies);
    }

    public void SpawnEnemy()
    {
        Instantiate(enemies[(int)Random.Range(0, enemies.Length)], new Vector3(xCoord,Random.Range(-betweenYCoord, betweenYCoord), 0), Quaternion.identity);
    }
}
