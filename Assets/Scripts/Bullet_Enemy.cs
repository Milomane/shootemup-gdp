﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Bullet_Enemy : MonoBehaviour
{
    public Vector2 direction;

    public float speed;

    public float dmg;
    
    void Start()
    {
        Destroy(gameObject, 5f);
    }
    
    void Update()
    {
        transform.Translate(direction * speed * 0.3f);
    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<PlayerSysteme>())
        {
            col.GetComponent<PlayerSysteme>().TakeDamage((int) dmg);
            Destroy(gameObject);
        }
    }
}
