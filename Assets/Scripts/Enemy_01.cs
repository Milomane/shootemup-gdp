﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_01 : Ennemy
{
    [Header("Ennemy_01")]
    public Vector2 direction;

    void Start()
    {
        
    }
    
    public override void Update()
    {
        base.Update();
        //movement
        transform.Translate(direction * speed * 0.1f);
    }
}
