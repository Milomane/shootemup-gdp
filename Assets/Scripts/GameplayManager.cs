﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
    public static GameplayManager gameplayManager;
    public static int lifeCount;
    
    
    void Awake()
    {
        if (gameplayManager == null)
        {
            gameplayManager = this;
            DontDestroyOnLoad(gameObject);
            lifeCount = 99;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public static void LifeLost()
    {
        
        lifeCount--;
    }
}
