﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerSysteme : MonoBehaviour
{
    public KeyCode up, down, left, right, shoot;
    public int speed;
    public int health, healthMax;
    public float fireRate, nextFire;
    
    public Camera mainCamera;
    private Vector2 screenBounds;
    private float objectWidth;
    private float objectHeight;
    public GameObject firePoint, bullet;

    public GameObject explosion;
    void Start()
    {
        health = healthMax;
        mainCamera.GetComponent<Camera>();
        screenBounds = mainCamera.ScreenToWorldPoint(new Vector3(x: Screen.width, y: Screen.height, mainCamera.transform.position.z));
        objectWidth = transform.GetComponent<SpriteRenderer>().bounds.extents.x;
        objectHeight = transform.GetComponent<SpriteRenderer>().bounds.extents.y;
        fireRate = nextFire;
        ScoreSystem.score = 0;
    }

    void Update()
    {
        if (Input.GetKey(up))
        {
            transform.Translate(Vector3.up * (speed * Time.deltaTime));
        }

        if (Input.GetKey(down))
        {
            transform.Translate(Vector3.down * (speed * Time.deltaTime));
        }

        if (Input.GetKey(left))
        {
            transform.Translate(Vector3.left * (speed * Time.deltaTime));
        }

        if (Input.GetKey(right))
        {
            transform.Translate(Vector3.right * (speed * Time.deltaTime));
        }
        fireRate -= Time.deltaTime;
        if (Input.GetKey(shoot) && fireRate<= 0)
        {
            Instantiate(bullet, firePoint.transform.position, Quaternion.identity);
            fireRate = nextFire;
        }
        if (health <= 0)
        {
            Instantiate(explosion, transform.position, explosion.transform.rotation);
            GameplayManager.LifeLost();
            gameObject.SetActive(false);
            SceneManager.LoadScene("InputName");
        }
    }
    void LateUpdate()
    {
        Vector3 viewPos = transform.position;
        viewPos.x = Mathf.Clamp(value: viewPos.x, min: screenBounds.x * -1 + objectWidth,
            max: screenBounds.x - objectWidth); //bloque pour ne pas depasser la cam
        viewPos.y = Mathf.Clamp(value: viewPos.y, min: screenBounds.y * -1 + objectHeight,
            max: screenBounds.y - objectHeight);
        transform.position = viewPos;
    }

    public void TakeDamage(int value)
    {
        health -= value;
    }
}
