﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    private PlayerSysteme player;
    public Transform barTransform;
    
    void Start()
    {
        player = FindObjectOfType<PlayerSysteme>();
    }
    
    void Update()
    {
        if (player.health > 0)
            barTransform.localScale = new Vector3((float)player.health/(float)player.healthMax,1,1);
        else
            barTransform.localScale = new Vector3(0,1,1);
    }
}
