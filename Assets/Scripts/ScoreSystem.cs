﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreSystem : MonoBehaviour
{
    public static int score;
    public TextMeshProUGUI scoreText;

    public void Update()
    {
        scoreText.text = "Score : " + score;
    }


    public static void AddScore(int amount)
    {
        score += amount;
    }
}
