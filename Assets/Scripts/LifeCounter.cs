﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LifeCounter : MonoBehaviour
{
    public TextMeshProUGUI lifeCounter;

    void Start()
    {
        lifeCounter.text = "Life : " + GameplayManager.lifeCount;
    }
}
